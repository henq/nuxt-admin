/*
 * @Author: your name
 * @Date: 2020-04-26 21:02:57
 * @LastEditTime: 2020-10-01 10:47:36
 * @LastEditors: Do not edit
 * @Description: In User Settings Edit
 * @FilePath: \source\plugins\i18n.js
 */
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import Languages from '@/i18n/locale';
Vue.use(VueI18n);
export default ({ app, store }) => {
    // 
  // Set i18n instance on app
  // This way we can use it in middleware and pages asyncData/fetch
  app.i18n = new VueI18n({
    locale:'zh-CN',//en-US zh-CN
    fallbackLocale:'en-US',
    messages: Languages,
    silentTranslationWarn: true
  });
  // app.i18n.path = (link) => {
  //   if (app.i18n.locale === app.i18n.fallbackLocale) {
  //     return `/${link}`;
  //   }
  //   return `/${app.i18n.locale}/${link}`;
  // }
}