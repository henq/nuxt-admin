import axios from 'axios';
import util from '@/libs/util.client';
import Setting from '@/setting';
import QS from 'querystring';
import { Message, Notice ,LoadingBar} from 'view-design';
// 创建一个错误
function errorCreate (msg) {
    const err = new Error(msg);
    errorLog(err);
    throw err;
}

// 记录和显示错误
function errorLog (err) {
    if(err&&err.response&&err.response.status==500){
        console.log(err.response)
        Message.error({
            content: err.response.data.error.message,
            duration: 5
        })
        return;
    }
    // 添加到日志
    $nuxt.$store.dispatch('admin/log/push', {
        message: '数据请求异常',
        type: 'error',
        meta: {
            error: err
        }
    });
    // 打印到控制台
    if (process.env.NODE_ENV === 'development') {
        util.log.error('>>>>>> Error >>>>>>');
        console.warn(err.response);
        console.log(err);
    }
    // 显示提示，可配置使用 iView 的 $Message 还是 $Notice 组件来显示
//     if (Setting.errorModalType === 'Message') {
//         Message.error({
//             content: err.message,
//             duration: Setting.modalDuration
//         });
//     } else if (Setting.errorModalType === 'Notice') {
//         Notice.error({
//             title: '提示',
//             desc: err.message,
//             duration: Setting.modalDuration
//         });
//     }
}

// 创建一个 axios 实例
const service = axios.create({
    baseURL: Setting.apiBaseURL,
    timeout: 5000, // 请求超时时间
    transformRequest:[(data)=>{
        function dateFormat(date, fmt) {
          if (null == date || undefined == date) return '';
          var o = {
            "M+": date.getMonth() + 1, //月份
            "d+": date.getDate(), //日
            "h+": date.getHours(), //小时
            "m+": date.getMinutes(), //分
            "s+": date.getSeconds(), //秒
            "S+": date.getMilliseconds() //毫秒
          };
          if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
          for (var k in o)
            if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
          return fmt;
        }
    
        Date.prototype.toJSON = function () {
          return dateFormat(this, 'yyyy-MM-ddThh:mm:ss.SSSZ')
        }
        return JSON.stringify(data)
      }]
});
// 设置post请求头
service.defaults.headers.post['Content-Type'] = 'application/json';
service.defaults.headers.put['Content-Type'] = 'application/json';
// 请求拦截器
service.interceptors.request.use(
    config => {
        // 在请求发送之前做一些处理
        const token = util.cookies.get('token');
        const linkId=localStorage.getItem("linkId");
        if(linkId){
            config.headers['linkId'] = linkId;
        }
        // 让每个请求携带token-- ['X-Token']为自定义key 请根据实际情况自行修改
        config.headers['Authorization'] = 'Bearer '+token;
        LoadingBar.start()
        return config;
    },
    error => {
        // 发送失败
        // console.log(error);
        Promise.reject(error);
    }
);

// 响应拦截器
service.interceptors.response.use(
    response => {
        LoadingBar.finish()
        // dataAxios 是 axios 返回数据中的 data
        const dataAxios = response.data;
        // 这个状态码是和后端约定的
        const { code } = dataAxios;
        // 根据 code 进行判断
        if (code === undefined) {
            // 如果没有 code 代表这不是项目后端开发的接口
            return dataAxios;
        } else {
            // 有 code 代表这是一个后端接口 可以进行进一步的判断
            switch (code) {
            case 0:
                // [ 示例 ] code === 0 代表没有错误
                return dataAxios.data;
            case 'xxx':
                // [ 示例 ] 其它和后台约定的 code
                errorCreate(`[ code: xxx ] ${dataAxios.msg}: ${response.config.url}`);
                break;
            default:
                // 不是正确的 code
                errorCreate(`${dataAxios.msg}: ${response.config.url}`);
                break;
            }
        }
      
    },
    error => {
        if (error && error.response) {
            switch (error.response.status) {
            case 400: error.message = '请求错误'; break;
            case 401: error.message = '未授权，请登录'; break;
            case 403: error.message = '拒绝访问'; break;
            case 404: error.message = `请求地址出错: ${error.response.config.url}`; break;
            case 408: error.message = '请求超时'; break;
            case 500: error.message = '服务器内部错误'; break;
            case 501: error.message = '服务未实现'; break;
            case 502: error.message = '网关错误'; break;
            case 503: error.message = '服务不可用'; break;
            case 504: error.message = '网关超时'; break;
            case 505: error.message = 'HTTP版本不受支持'; break;
            default: break;
            }
        }        
        errorLog(error);
        LoadingBar.error()
        // console.warn("Erro:",error)
        return Promise.reject(error);
    }
);
let api=service.get;
 let get=(url,params)=>{
    if(params){
        return api(url,{"params":params})
    }else{
        return api(url)
    }    
}

service.get=get;
export default service;
