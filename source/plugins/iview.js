import Vue from 'vue'
import ViewUI from 'view-design'
import clickOutside from 'view-design/src/directives/clickoutside';
import transferDom from 'view-design/src/directives/transfer-dom';
// 插件
import util from '@/libs/util.client';
import { includeArray } from '@/libs/system';

import VueDND from 'awe-dnd' //拖拽排序
import Henq from '@/libs/henq';
// import iViewPro from '@/libs/iViewPro/iview-pro.min.js';
// import  '@/libs/iViewPro/iview-pro.css';
import iViewPro from '../iview-pro/src/index.js';
import '@/libs/iview-pro/iview-pro.css';
import '@/styles/index.less';
import '@/assets/css/henq.less';
import request from '@/plugins/request'
import Highlight  from '@/libs/highlight.js' //导入代码高亮文件
// import 'highlight.js/styles/monokai-sublime.css'
// 内置组件
import iLink from '@/components/link';
import formCreate from '@form-create/iview4'
Vue.component("iLink", iLink)
const requireComponent = require.context(
  // 其组件目录的相对路径
  '../components/page',
  // 是否查询其子目录
  false,
  // 匹配vue后缀文件名的文件
  /\.(vue|js)$/
)
// 遍历获取到的文件名，依次进行全局注册
requireComponent.keys().forEach(fileName => {
          // 获取组件配置(实例)
          const componentConfig = requireComponent(fileName)
          // 获取组件的 PascalCase 命名(eg: MYHeader)
          const componentName =  fileName.replace(/^\.\/(.*)\.\w+$/, '$1');
          const name=componentName.charAt(0).toUpperCase()+componentName.slice(1);
           // 全局注册组件
           Vue.component(
            name,
            // 如果这个组件选项是通过 `export default` 导出的，
            // 那么就会优先使用 `.default`，
            // 否则回退到使用模块的根。
            componentConfig.default || componentConfig
        )
    })
Vue.prototype.$u = Henq;
Vue.prototype.$api = request;
//注入mock
require("../mock")
export default ({
  app,
  store
}) => {
  // Vue.use(VueI18n)
  Vue.use(VueDND)
  Vue.use(formCreate)
  // app.i18n = new VueI18n({
  //   locale: 'zh-CN',
  //   messages: Languages
  // });
  // app.i18n.locale = "zh-CN"
  Vue.use(ViewUI, {
    // locale:'zh-CN',
    // i18n: (key, value) => app.i18n.t(key, value)
    i18n: app.i18n
  })
    Vue.use(Highlight);
  Vue.use(iViewPro);
  Vue.prototype.$log = {
    ...util.log,
    push(data) {
      if (typeof data === 'string') {
        // 如果传递来的数据是字符串
        // 赋值给 message 字段
        // 为了方便使用
        // eg: this.$log.push('foo text')
        store.dispatch('admin/log/push', {
          message: data
        });
      } else if (typeof data === 'object') {
        // 如果传递来的数据是对象
        store.dispatch('admin/log/push', data);
      }
    }
  }
  Vue.config.errorHandler = function (error, instance, info) {
    Vue.nextTick(() => {
      // store 追加 log
      store.dispatch('admin/log/push', {
        message: `${info}: ${error.message}`,
        type: 'error',
        meta: {
          error
          // instance
        }
      });
      // 只在开发模式下打印 log
      if (process.env.NODE_ENV === 'development') {
        util.log.capsule('iView Admin', 'ErrorHandler', 'error');
        util.log.error('>>>>>> 错误信息 >>>>>>');
        console.log(info);
        util.log.error('>>>>>> Vue 实例 >>>>>>');
        console.log(instance);
        util.log.error('>>>>>> Error >>>>>>');
        console.log(error)
      }
    })
  }

  Vue.directive('auth', {
    inserted(el, binding, vnode) {
      const { value } = binding;
      const access = store.state.admin.user.info.access;

      if (value && value instanceof Array && value.length && access && access.length) {
        const isPermission = includeArray(value, access);
        if (!isPermission) {
          el.parentNode && el.parentNode.removeChild(el);
        }
      }
    }
  })
  Vue.directive('paste', {
    bind(el, binding, vnode) {
      el.addEventListener('paste', function (event) { //这里直接监听元素的粘贴事件
        binding.value(event)
      })
    }
  })
  // 防重复点击(指令实现)
  Vue.directive('double', {
    inserted(el, binding) {
      el.addEventListener('click', () => {
        if (!el.disabled) {
          el.disabled = true
          setTimeout(() => {
            el.disabled = false
          }, binding.value || 3000)
        }
      })
    }
  })
  Vue.directive("clickOutside",clickOutside)
  Vue.directive("transferDom",transferDom)
  
  // Vue.directive('paste', {
  //   bind(el, binding, vnode) {
  //     el.addEventListener('paste', function (event) { //这里直接监听元素的粘贴事件
  //       binding.value(event)
  //     })
  //   }
  // })
  // console.warn("this:",app.i18n.locale,app)

}