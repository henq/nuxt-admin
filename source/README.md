<!--
 * @Author: Henq
 * @Date: 2020-04-26 21:02:58
 * @LastEditTime: 2020-11-26 22:58:16
 * @LastEditors: Do not edit
 * @FilePath: \source\README.md
 * @Description: 
-->
# admin

> this is init project

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
