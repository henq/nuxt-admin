const path = require('path')

const resolve = dir => {
  return path.join(__dirname, dir)
}
const webpack = require('webpack')
// 增加环境变量
process.env.VUE_APP_VERSION = require('./package.json').version;
process.env.VUE_APP_BUILD_TIME = require('dayjs')().format('YYYY-M-D HH:mm:ss');

export default {
   // mode: 'spa',
   ssr:false,
  /*
   ** Headers of the page
   */
  head: {
    title: process.env.npm_package_name || '',
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    script: [{
      src: '/config.js'
    }],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.ico'
    }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#fff'
  },
  /*
   ** Global CSS
   */
  css: [
    // 'iview/dist/styles/iview.css',
    // '@assets/css/common.css',
    // {src: '@assets/css/common.less', lang: 'less'},
    
    './libs/iview-pro/iview-pro.css',
       {
      src: '@assets/css/henq.less',
      lang: 'less'
    },
    // {
    //   src: '@assets/css/iview-reset.less',
    //   lang: 'less'
    // },
    // {
    //   src: '@assets/css/base.less',
    //   lang: 'less'
    // },
  ],
  router: {              // customize nuxt.js router (vue-router).
    middleware: 'i18n'   // middleware all pages of the application
  },
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    {src:'@/plugins/iview',ssr:false},
    {src:'@/plugins/route',ssr:false},
    '~/plugins/i18n.js'
    // '@/plugins/route'
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/style-resources',
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {},
  server: {
    port: 7000, // default: 3000
    host: '0.0.0.0' // default: localhost,
  },
  generate: {
    // routes: ['/', '/about', '/fr', '/fr/about']
    routes:['/']
  },
  /*
   ** Build configuration
   */
  build: {
    vendor: ['vue-i18n'], // webpack vue-i18n.bundle.js
    /*
    ** You can extend webpack config here
    */
   loaders: {
    less: {
      javascriptEnabled: true
    }
  },
  extend(config, ctx) {
    config.resolve.alias['vue$'] = 'vue/dist/vue.esm.js'
   
    config.node = {
      fs: "empty"
    }
  }
  ,
  chainWebpack: config => {
    // ...
    config.node
     .set('__dirname', true) // 同理
     .set('__filename', true)
   },
    extend (config, ctx) {
      config.resolve.alias['_c']=path.resolve(__dirname, 'components')
      config.node = {
        console: true,
        fs: 'empty',
      }
      // config.module.rules.push({
      //   enforce:'pre',
      //   test: /\.vue$/,
      //   loader: 'iview-loader',
      //   options: {
      //     prefix: false
      // }
      // });
    }
    // ,
  //   chainWebpack: config => {
  //     // 不编译 iView Pro
  //     config.module
  //     .rule('js')
  //     .test(/\.jsx?$/)
  //     .exclude
  //     .add(path.resolve(__dirname,'libs/iview-pro'))
  //     .end();
  // },
  ,plugins: [
    new webpack.ProvidePlugin({'window.Quill':'quill' })
  ]
  }
 
}
