import request from '@/plugins/request';

export function AccountLogin (data) {   
    return request.post("/account/login",data);
}

export function AccountRegister (data) {
    return request({
        url: '/api/register',
        method: 'post',
        data
    });
}
