import Api from '@/plugins/request'
export default (table)=>{
    let methods={
            info(){
            return Api.get(`${table}/info`);
            },
        paged(params){
            return Api.post(`${table}/paged`,params);
            },
            list(params){
                return Api.post(`${table}/list`,params);
                },
            get(params){
            return Api.get(`${table}/get`,params);
            },
            create(params){
            return Api.post(`${table}/create`,params);
            },
            update(params){
            return Api.post(`${table}/update`,params);
            },
            link(params){
                return Api.post(`${table}/link`,params);
                },
            delete(id) {        
            return Api.delete(`${table}/deletes`,{params:{id:id}});
            },
            deletes(params) {
            return Api.post(`${table}/batchdelete`,params);
            }
    };
    
    return methods;
}