import api from '@/plugins/request'

const u = (path) => `/api/Dictionary/${path}`
export default {
  getChildren(id) {
    var paras = {
      "sortBy": "Priority",
      "isDesc": false,
      "conditions": [{
        "fieldName": "upId",
        "fieldValue":id,
        "conditionalType": "Equal"
      }]
    }
    if(!id){
      paras = {
        "sortBy": "Priority",
        "isDesc": false,
        "conditions": [{
          "fieldName": "typeId",
          "fieldValue":0,
          "conditionalType": "Equal"
        }]
      }
    }
    return api.post(u("list"), paras)
  },
  getTree() {
    var paras = {
        "sortBy": "Priority",
        "isDesc": false,
        "conditions": [{
          "fieldName": "typeId",
          "fieldValue":"0,1,2",
          "conditionalType": "In"
        }]
      }
      return api.post(u("list"), paras)
  },
  setColor(paras) {
    return api.post(u("SetColor"),paras)
  },
  setSort(paras) {
    return api.post(u("SetSort"), paras)
  },
  save(paras) {
    return api.post(u("Create"), paras)
  },
}
