import Api from '@/plugins/request'
const technologyUrl = "/api/"
export default {
  index: `${technologyUrl}dbentitydefined/page`,
  yi(params) {
    return Api.get(`${technologyUrl}common/fanyi`, params)
  },
  paged(params) {
    return Api.post(`${technologyUrl}dbentitydefined/page`, params);
  },
  get(params) {
    return Api.get(`${technologyUrl}dbentitydefined/get`, params);
  },
  create(params) {
    return Api.post(`${technologyUrl}dbentitydefined/create`, params);
  },
  update(params) {
    return Api.post(`${technologyUrl}dbentitydefined/update`, params);
  },
  check(params) {
    return Api.get(`${technologyUrl}DbEntityDefined/CheckTable`, params);
  },
  createTable(params) {
    return Api.get(`${technologyUrl}DbEntityDefined/CreateTable`, params);
  },
  updateTable(params) {
    return Api.get(`${technologyUrl}DbEntityDefined/UpdateTable`, params);
  },
  delete(id) {
    return Api.delete(`${technologyUrl}dbentitydefined/deletes`, {
      params: {
        id: id
      }
    });
  },
  deletes(params) {
    return Api.post(`${technologyUrl}dbentitydefined/batchdelete`, params);
  },
  links() {
    var params = {
      "sortBy": "id",
      "isDesc": true,
      "conditions": [],
      "pageSize": 50
    };
    return Api.post(`${technologyUrl}DbLink/List`,params)
  }
}
