/*
 * @Author: your name
 * @Date: 2020-05-04 20:09:41
 * @LastEditTime: 2020-05-16 00:07:43
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \source\pages\code\DBLink\api.js
 */
import Api from '@/plugins/request'
const technologyUrl = "/api/"
export default {
  index: `${technologyUrl}db/gettables`,
  getLinks(params) {
    return Api.get(`${technologyUrl}db/GetLinks`, params);
  },
  createTable(params) {
    return Api.post(`${technologyUrl}db/CreateTable`, params);
  },
  getTables(params) {
    return Api.get(`${technologyUrl}db/gettables`, params);
  },
  getColumnInfos(params) {
    return Api.get(`${technologyUrl}db/getColumnInfos`, params);
  },
  create(params) {
    return Api.post(`${technologyUrl}dblink/create`, params);
  },
  update(params) {
    return Api.post(`${technologyUrl}dblink/update`, params);
  },
  link(params) {
    return Api.post(`${technologyUrl}dblink/link`, params);
  },
  addTableRemark(params) {
    return Api.post(`${technologyUrl}db/addTableRemark`, params);
  },
  tableImport(table) {
    return Api.get(`${technologyUrl}dbEntityDefined/tableImport`, table);
  },
  delete(id) {
    return Api.delete(`${technologyUrl}dblink/deletes`, {
      params: {
        id: id
      }
    });
  },
  deletes(params) {
    return Api.post(`${technologyUrl}dblink/batchdelete`, params);
  }
}
