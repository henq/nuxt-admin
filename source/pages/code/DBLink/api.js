/*
 * @Author: your name
 * @Date: 2020-05-04 20:09:41
 * @LastEditTime: 2020-05-05 18:23:44
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \source\pages\code\DBLink\api.js
 */
import Api from '@/plugins/request'
const technologyUrl="/api/"
export default {
    index:`${technologyUrl}dblink/page`,
    paged(params){
    return Api.post(`${technologyUrl}dblink/page`,params);
    },
    get(params){
    return Api.get(`${technologyUrl}dblink/get`,params);
    },
    create(params){
    return Api.post(`${technologyUrl}dblink/create`,params);
    },
    update(params){
    return Api.post(`${technologyUrl}dblink/update`,params);
    },
    link(params){
        return Api.post(`${technologyUrl}dblink/link`,params);
        },
    delete(id) {        
    return Api.delete(`${technologyUrl}dblink/deletes`,{params:{id:id}});
    },
    deletes(params) {
    return Api.post(`${technologyUrl}dblink/batchdelete`,params);
    }
    }