// import createVuexAlong from 'vuex-along'
// import datas from "@/libs/dictionary";
import Api from '@/plugins/request'
export const state = () => ({
  counter: 0,
  dictionary: [], //所有字典项
  userInfo: {
    userId: 0,
    userName: ""
  }
})
export const getters={
  dictionaryByKey: (state) => (key) => {
    let result = [];
    let items = state.dictionary.filter(u=>{
      return u.code==key;
    });
    if (items&&items.length==1) {
      // console.info("items",items)
      return items[0].children;
    }
    return result;
  }

}
export const mutations = {
  setDictionary(state, dictionary) {
    state.dictionary = dictionary;
  },
  increment(state) {
    state.counter++
  }
}
export const actions = {
  async loadDictionary({
    commit
  }) {
      let url = `/api/Dictionary/GetAll`
      let { result } = await Api.get(url);
    commit("setDictionary", result);
  }
}
// export const plugins= [
//     createVuexAlong({
//       // 设置保存的集合名字，避免同站点下的多项目数据冲突
//       name: "hyhmes",
//       local: {
//         list: ["hyhmes"],
//         // 过滤模块 ma 数据， 将其他的存入 localStorage
//         isFilter: true,
//       },
//       session: {
//         // 保存模块 ma 中的 a1 到 sessionStorage
//         list: ["hyhmes.session"],
//       },
//     }),
//   ]
//设置 strict 为不严格模式，即可在actions中修改state 
export const strict = false;
