export const state=()=>({
    info:{}
})
export const mutations={
    increment(state){
        state.counter++
    },
    setUser(state,info){
        state.info=info;
    }
}
export const actions={
     /**
         * @description 设置用户数据
         * @param {Object} state vuex state
         * @param {Object} dispatch vuex dispatch
         * @param {*} info info
         */
        set ({ state, dispatch,commit}, info) {
            return new Promise(async resolve => {
                // store 赋值
                // state.info = info;
                commit("setUser",info)
                // 持久化
                await dispatch('admin/db/set', {
                    dbName: 'sys',
                    path: 'user.info',
                    value: info,
                    user: true
                }, { root: true });
                // end
                resolve();
            })
        },
        /**
         * @description 从数据库取用户数据
         * @param {Object} state vuex state
         * @param {Object} dispatch vuex dispatch
         */
        load ({ state, dispatch ,commit}) {
            return new Promise(async resolve => {
                // store 赋值
               let info = await dispatch('admin/db/get', {
                    dbName: 'sys',
                    path: 'user.info',
                    defaultValue: {},
                    user: true
                }, { root: true });
                commit("setUser",info)
                // end
                resolve();
            })
        }
}