import low from 'lowdb';
import LocalStorage from 'lowdb/adapters/LocalStorage';
// import FileSync from 'lowdb/adapters/FileSync';

// const adapter = new LocalStorage('admin');
// // const adapter = new FileSync('db.json');
// const db = low(adapter);

// db
//     .defaults({
//         sys: {},
//         database: {}
//     })
//     .write();

// export default db;
export default function () {
    const adapter = new LocalStorage('admin');
    // const adapter = new FileSync('db.json');
    const db = low(adapter);

    db
        .defaults({
            sys: {},
            database: {}
        })
        .write();
        return db;
}
