/*
 * @Author: Henq
 * @Date: 2020-11-23 23:53:09
 * @LastEditTime: 2020-12-12 12:43:07
 * @LastEditors: Do not edit
 * @FilePath: \source\libs\henq.js
 * @Description: henq的公用方法
 */
let henq = {};
let pdfInfo = ''
/**
 * @description: 克隆方法
 * @param {*} obj
 * @return {*} new obj
 */
henq.clone = (obj) => {
  var that = henq
  var o
  if (typeof obj === 'object') {
    if (obj === null) {
      o = null
    } else {
      if (obj instanceof Array) {
        o = []
        for (var i = 0, len = obj.length; i < len; i++) {
          o.push(that.clone(obj[i]))
        }
      } else {
        o = {}
        for (var j in obj) {
          o[j] = that.clone(obj[j])
        }
      }
    }
  } else {
    o = obj
  }
  return o
}
henq.merge = () => {
  Object.assign(argments)
}
/**
 * @description: 空检测 包含null '',undefined
 * @param {*} u
 * @return {*}
 */
henq.isNull = u => {
  if (u === null) {
    return true;
  }
  if (u === undefined) {
    return true
  }
  if (u === '') {
    return true
  }
  return false;
}
henq.isArray = u => {
  return Object.prototype.toString.call(u) == '[object Array]';
}
henq.toIntArray = u => {
  if (henq.isNull(u)) {
    return [];
  }
  var arrs = u.split(',')
  var result = [];
  arrs.map(p => {
    result.push(parseInt(p));
  })
  return result;
}

/**
 * @description: 生成guid
 * @param {*}
 * @return {*}
 */
henq.guid = () => {
  var s = [];
  var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  for (var i = 0; i < 36; i++) {
    s[i] = chars.substr(Math.floor(Math.random() * 0x10), 1);
  }
  s[14] = "4"; // bits 12-15 of the time_hi_and_version field to 0010
  s[19] = chars.substr((s[19] & 0x3) | 0x8, 1); // bits 6-7 of the clock_seq_hi_and_reserved to 01
  s[8] = s[13] = s[18] = s[23] = "-";
  return s.join("");
}
henq.toTime = (date, fmt = 'yyyy-MM-dd hh:mm:ss') => {
  if (!(date instanceof Date)) {
    console.log(date)
    date = new Date(date);

  }
  var o = {
    "M+": date.getMonth() + 1, //月份 
    "d+": date.getDate(), //日 
    "h+": date.getHours(), //小时 
    "m+": date.getMinutes(), //分 
    "s+": date.getSeconds(), //秒 
    "q+": Math.floor((date.getMonth() + 3) / 3), //季度 
    "S": date.getMilliseconds() //毫秒 
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (date.getFullYear() + "").substr(4 - RegExp.$1.length));
  for (var k in o)
    if (new RegExp("(" + k + ")").test(fmt))
      fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
  return fmt;
}
henq.toDate = (date) => {
  return henq.toTime(date, 'yyyy-MM-dd');
}
henq.getDate = (strDate) => {
  var date = eval('new Date(' + strDate.replace(/\d+(?=-[^-]+$)/,
    function (a) {
      return parseInt(a, 10) - 1;
    }).match(/\d+/g) + ')');
  return date;
}
henq.isInt = (val) => {
  var regPos = /^\d+$/; // 非负整数 
  var regNeg = /^\-[1-9][0-9]*$/; // 负整数
  if (regPos.test(val) || regNeg.test(val)) {
    return true;
  } else {
    return false;
  }
}
henq.isNumber = (val) => {
  var regPos = /^\d+(\.\d+)?$/; //非负浮点数
  var regNeg = /^(-(([0-9]+\.[0-9]*[1-9][0-9]*)|([0-9]*[1-9][0-9]*\.[0-9]+)|([0-9]*[1-9][0-9]*)))$/; //负浮点数
  if (regPos.test(val) || regNeg.test(val)) {
    return true;
  } else {
    return false;
  }
}
henq.toHump=(str)=>{
  var result= str.replace(/-(\w)/g,($0,$1)=>{
    return $1.toUpperCase();
  })
  return result.charAt(0).toLowerCase()+result.slice(1);
}
henq.toTree = (list, rootId, format, parentFiledName) => {
  var upId = parentFiledName || "upId";
  let i = 0;

  function toTree(data, parentId, level) {
    let parents = data.filter(u => {
      return u[upId] == parentId
    })

    if (parents && parents.length > 0 && level < 10) {
      level++
      parents.map(u => {
        if (format) {
          format(u, data)
        }
        u.children = toTree(data, u.id, level)
      })
    }
    return parents;
  }
  return toTree(list, rootId, i)
}
henq.treeToList = (tree) => {
  let list = [];
  function treeToList(data) {
    data.map(u => {
      if (u.children) {
        treeToList(u.children, u)
      }
      let copy = henq.clone(u);
      delete copy.delete;
      copy.parent = u;
      list.push(copy);
    })
  }
  treeToList(tree, null)
  return list;
}
/**
 *  Equal = 0,
        Like = 1,
        GreaterThan = 2,
        GreaterThanOrEqual = 3,
        LessThan = 4,
        LessThanOrEqual = 5,
        In = 6,
        NotIn = 7,
        LikeLeft = 8,
        LikeRight = 9,
        NoEqual = 10,
        IsNullOrEmpty = 11,
        IsNot = 12,
        NoLike = 13
* @function 搜索条件转换函数
* @description 把对象转换为数组
* @param grid {Ext.Grid.Panel} 需要合并的Grid
* @return arry
* @author henq 2015/07/21 
* @example
* _________________ _________________
* | 年龄 | 姓名 | | 年龄 | 姓名 |
* ----------------- mergeCells(grid,[0]) -----------------
* | 18 | 张三 | => | | 张三 |
* ----------------- - 18 ---------
* | 18 | 王五 | | | 王五 |
* ----------------- -----------------
*/
henq.conditions = (conditions) => {
  var where = [];
  if (conditions) {
    let ret = henq.type(conditions);
    if (ret === "array") {
      return conditions;
    } else if (ret === "object") {
      Object.keys(conditions).forEach((u) => {
        let type = henq.type(conditions[u])
        if (type === "object") {
          let v = conditions[u].value;
          let op = conditions[u].op;
          if (!henq.isNull(v)) {
            if (op == "Range" && Array.isArray(v)) {
              let times = [];
              v.map((u) => {
                if (!this.$u.isNull(u)) {
                  times.push(this.$u.toTime(u));
                }
              });
              v = times.join(",");
            } else if (op.indexOf("In") > -1 && Array.isArray(v)) {
              v = v.join(",");
            }
            if (!henq.isNull(v)) {
              where.push({
                fieldName: u,
                fieldValue: v,
                conditionalType: op,
              });
            }
          }
        } else {
          let v = conditions[u];
          if (!henq.isNull(v)) {
            where.push({
              fieldName: u,
              fieldValue: v,
              conditionalType: "Equal",
            });
          }
        }
      });
    }
  }
  return where;
}
/**
* @function 返回对象类型
* @description 
* @param target 
* @return string
* @author henq 20201121
* @example
* 
*/
henq.type = (target) => {
  var ret = typeof (target);//首先大致确定参数类型确定，
  var template = {//定义一个类装对象的类型
    "[object Array]": "array",
    "[object Object]": "object",
    "[object Number]": "number",
    "[object Boolean]": "boolean",
    "[object String]": "string"
  }
  if (target === null) {
    return "null";
  } else if (ret == "object") {//这里对typeof返回的object区分[]、{}
    var str = Object.prototype.toString.call(target);
    return template[str];//用obj[str],obj.str会被当成属性而不是变量
  } else {
    return ret;//不传参返回undefined
  }
}
//导出pdf
henq.outPdf = (ele, fileName) => {
  // document.head.innerHTML =
  //   '<meta charset="utf-8">\n' +
  //   ' <title> ' +
  //   fileName +
  //   '</title>\n' +
  //   ' <meta name="format-detection" content="telephone=no">\n' +
  //   ' <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">\n' +
  //   ' <meta name="viewport" content="width=device-width,initial-scale=1.0">\n' +
  //   '<link  rel="stylesheet" href="/static/iviewRe.css"/>\n' +
  //   '<link  rel="stylesheet" href="/static/iview-reset.css"/>\n'+
  //   '<link  rel="stylesheet" href="/static/base.css"/>';
  //henq.getPdfBody(document.querySelector(ele).outerHTML);
  //document.body.innerHTML = document.querySelector(ele).outerHTML
  //转异步 等待dom元素渲染（样式）完毕在打印
  sessionStorage.removeItem('pdf')
  sessionStorage.removeItem('pdfTitle')
  sessionStorage.setItem("pdfTitle", fileName)
  sessionStorage.setItem("pdf", document.querySelector(ele).outerHTML)
  setTimeout(() => {
    window.open("/static/pdf.html", '_blank');
  }, 1000)
}
export default henq;