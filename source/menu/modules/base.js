const pre = '/base/';

export default {
  path: '/base',
  title: '基础数据',
  header: 'home',
  icon: 'md-speedometer',
  children: [{
      path: `${pre}`,
      title: '主控台'
    }
    ,
    {
        path: '/base/user',
        title: '用户管理',
        header: 'home'
    },
  ]
}
