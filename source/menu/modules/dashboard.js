const pre = '/dashboard/';

export default {
  path: '/dashboard',
  title: '基础管理',
  header: 'home',
  icon: 'md-speedometer',
  children: [{
      path: `${pre}console`,
      title: '主控台'
    }
  ]
}
