/*
 * @Author: your name
 * @Date: 2020-04-26 20:57:54
 * @LastEditTime: 2020-12-05 09:38:40
 * @LastEditors: Do not edit
 * @Description: In User Settings Edit
 * @FilePath: \source\menu\sider.js
 */
// 菜单，侧边栏
import dashboard from './modules/dashboard';

// 系统
import log from './modules/log';

export default [
  dashboard,
  log,
  {
    path: '',
    title: '项目构建',
    header: 'home',
    icon: 'md-person',
    children: [{
        path: '/hello',
        title: '前端1日志',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/base/dictionary',
        title: '数据字典',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/code/cache',
        title: '缓存管理',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/code/DBLink',
        title: '链接管理',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/code/database',
        title: '数据库管理',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/code/DbEntityDefined',
        title: '实体管理',
        header: 'home',
        icon: 'md-person'
      },
     
      // {
      //   path: '/bus',
      //   title: '对象管理',
      //   header: 'home',
      //   icon: 'md-person'
      // }, 
     
      {
        path: '/doc',
        title: '文档管理',
        header: 'home',
        icon: 'md-person'
      },    
    ]
  },
  {
    path: '/example',
    title: '示例',
    header: 'home',
    icon: 'md-person',
    children: [
      {
        path: '/example',
        title: '练习',
        header: 'home',
        icon: 'md-person'
      },
      {
        path: '/example/english',
        title: 'english',
        header: 'home',
        icon: 'md-person'
      }
    ]
  }
];
