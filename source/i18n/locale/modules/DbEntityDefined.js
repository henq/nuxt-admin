/*
 * @Author: your name
 * @Date: 2020-05-05 11:00:27
 * @LastEditTime: 2020-11-08 14:03:59
 * @LastEditors: Do not edit
 * @Description: In User Settings Edit
 * @FilePath: \source\i18n\locale\modules\DbEntityDefined.js
 */
export default {
  'zh-CN': {
    DbEntityDefined: {
      base: '基类',
      dbFullName: '全名',
      columns: '列',
      methods: '方法',
      nameRule: '命名规则',
      entityName: '类名',
      dbTableName: '表名',
      tableDescription: '名称',   
      status: '状态',
    },
    'en-US': {
        DbEntityDefined: {
        base: '基类',
        dbFullName: '全名',
        columns: '列',
        methods: '方法',
        nameRule: '名规则',
        entityName: '类名',
        dbTableName: '表名',
        tableDescription: '名称',    
        status: '状态',
      }
    }
  }
}
