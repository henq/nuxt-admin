/*
 * @Author: your name
 * @Date: 2020-05-05 11:00:27
 * @LastEditTime: 2020-05-05 12:14:17
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \source\i18n\locale\modules\DBLink.js
 */
export default {
    'zh-CN':{
        DBLink:{
            name:'名称',
            link:'链接',
            dbType:'数据类型',
            note:'备注'       
                 }
    },
    'en-US':{
        DBLink:{
            name:'名称',
            link:'链接',
            dbType:'数据类型',
            note:'备注'       
                 }
    }


}
